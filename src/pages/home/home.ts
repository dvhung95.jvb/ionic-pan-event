import { Component, ViewChild, ElementRef} from '@angular/core';
import { Content } from 'ionic-angular';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    @ViewChild(Content) content: Content;
    @ViewChild('tabContent') tabContent: ElementRef;
    // @ViewChild('listTab') listTab: ElementRef;

    activeTab = 'lastest';
    activeTabs = ['lastest'];
    tabList = [
        {
            name: 'Lastest',
            value: 'lastest', 
        },
        {
            name: 'Popular',
            value: 'popular', 
        },
        {
            name: 'Bestseller',
            value: 'bestseller', 
        },
        {
            name: 'blog',
            value: 'blog', 
        }
    ];
    panX = 0;
    tabWidth = 0;
    tabMoveX = 0;

    constructor(public navCtrl: NavController) {
    }

    ngOnInit() {
        
    }

    ionViewDidLoad() {
        this.tabWidth = window.innerWidth * 4;
        setTimeout(() => {
            this.tabContent.nativeElement.style.height = document.querySelector('.tab-item.active').clientHeight + 'px';
        }, 300);
    }

    panEvent(e) {
        this.panX += e.srcEvent.movementX;
        this.tabMoveX += e.srcEvent.movementX;
    }

    panEnd(e) {
        let targetX = 200;
        if( this.content.contentWidth / 2 < 400) {
            targetX = 100;
        }
        if (Math.abs(this.panX) >= targetX) {
            const current = this.activeTab;
            const found = this.tabList.findIndex(function(e) {
                return e.value == current;
            });
            if (this.panX < 0 && this.tabList[found + 1]) {
                this.activeTab = this.tabList[found + 1].value;
                this.tabMoveX = -window.innerWidth * (found + 1);
            } else if (this.panX > 0 && this.tabList[found - 1]) {
                this.activeTab = this.tabList[found - 1].value;
            }
            this.panX = 0;
            if (this.activeTabs.indexOf(this.activeTab) === -1) {
                this.activeTabs.push(this.activeTab);
            }
        }
        this.checkActiveTabPos();
    }

    addActiveTabs(tab) {
        this.activeTab = tab;
        if (this.activeTabs.indexOf(tab) === -1) {
            this.activeTabs.push(tab);
        }
        this.checkActiveTabPos();
    }

    checkActiveTabPos() {
        const tab = this.activeTab;
        const pos = this.tabList.findIndex(function(e) {
            return e.value == tab;
        });
        this.tabMoveX = -window.innerWidth * pos;
        setTimeout(() => { 
            this.tabContent.nativeElement.style.height = document.querySelector('.tab-item.active').clientHeight + 'px';
        }, 300);
    }

    isInActiveTabs(tab) {
        if (this.activeTabs.indexOf(tab) !== -1) {
            return true;
        }
        return false;
    }

}
